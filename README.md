# The Ananke Programming Language

This repository contains the source code for the Ananke Programming Language. See also [ananke.dev](https://ananke.dev).

## Structure:

- **docs** contains all documentation. Current state: bad, incomplete, outdated, contradictory.
- **web** contains the source code of ananke.dev, TBD
- **stdlib** contains the Ananke source code for the Ananke standard library, TBD
- **protoypes** contains various bits and pieces of grammar, interpreters, compilers, assemblers, and other experiments
- **tests** testing framework, TBD

## Contributing:

I have finished all major syntax decisions. In the following weeks, all documentation and example code will be updated to their (hopefully) final form.

Once that is complete, the project will be open to contributions.

The project is always open to suggestions.
