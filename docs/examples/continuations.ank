fun main(){
	var i = 0
	fun continuation(){
		printl("got \{x}")
		i += 1
		continue
	}
	; this will loop forever
	continuation()

	/. Since the compiler infers the need for continuations and resumption to be stackfull, you can jump back and forth without worrying about corrupting the stack. This allows one to emulate "algebraic effects": ./
	; exception is a function that takes a resumption
	fun thrower(int val, fun(fun(int)) exception){
		if !val {
			; "throw" an exception
			exception( fn(int correction){
				val = correction
				resume
			} )
			; here, exception() is a continuation
			; that will continue execution somewhere else (namely in the handler)
			; which is why we pass it a continuation
			; that continues execution right here
			; the handler can then come up with a correction and call us back
			; (or still exit the program)
		}
		; sure, both could be callbacks. But they could also be continuations, which afford a bit more freedom in how you lay out the code.
		; we reach this point only if the value is correct, either at first, or by correction
		printl("we got \{val}!")
	}

	/. Stackfull continuations also allow one to implement coroutines: the `yield` keyword is really just returning a continuation that leads back into the coroutine after that point. ./
	fun coro(){
		return 3, fun(){continue}
		return 4, fun(){continue}
		return 5, fun(){continue}
		return 6, nil
	}

	/. think of it like this:
	fn coro(){
		return 3, fn(){
			return 4, fn(){
				return 5, fn(){
					return 6, nil
				}
			}
		}
	}
	./

	type coroT = i32, fun()(coroT/void)
	; a pair of i32
	; and a function that either returns another coro
	; or returns nil

	var x, reentry = _, coro {
		x, reentry = reentry()
		printl("Got \{x}!")
	} until reentry == nil
}


