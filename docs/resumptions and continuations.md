`resume` resumes the control flow _of_ the parent function.


`continue` continues the control flow _into_ the parent function.

The difference is that resume does not alter the return address of the parent function, which makes it suitable for resumable exceptions, whose throwers should always return back to their callers, and not to the exception handler.

```ananke
fun task(fun throw){
	if error
		throw( fun(){resume} )

	; resumption will resume control flow on this line
	; to where this activation record returns has not been modified, which means that...
	ret 42
}

fun main(){
	fun catch(fun resumption){
		if not fixable
			crash()
		resumption()
		; the "ret 42" will not return control flow to here, but rather...
	}

	let fortytwo = task(catch); to here, at the original call site
}
```

A continuation on the other hand can be useful for coroutines, where you want to return to the most recent caller instead of the first caller.

```ananke
fun coro(){
	ret 1, fun(){continue}
	; continuation will resume control flow on this line
	; to where this activation record returns has been modified.
	; in this case it doesn't matter, since we always call the continuation at the same line of code...
	ret 2, fun(){continue}
	ret 3, fun(){continue}
	ret 4, nil
}

fun main(){
	var continuation = coro
	var value = _
	var n = 0
	{
		continuation,value = continuation(); namely here
		print("value \{n} is \{value}!")
		n++
	} until continuation == nil
}
```

Here is a functionally equivalent rewrite that doesn't use `continue` nor `resume`. Note the differences in `coro`, `task` and `catch`. Note how despite the semantics of `continue` being arguably _more_ complex, it's transformation is actually trivial, while `resume`, which is concentpually simpler, has a more involved transformation, including the function signature of `catch` being modified to return something.

```ank
fun coro(){
	ret 1, fun(){
		ret 2, fun(){
			ret 3, fun(){
				ret 4, nil
			}
		}
	}
}

fun task(fun throw){
	fun task_rest(){
		ret 42
	}

	if error
		ret throw(task_rest)	

	ret task_rest()
}

fun main(){
	var c,v,n = coro,_,0 {
		c,v = c();
		print("value \{n} is \{v}!")
		n++
	} until c == nil

	fun catch(fun res){
		if not fixable
			crash()
			
		ret res()
	}

	let fortytwo = task(catch)
}
```
