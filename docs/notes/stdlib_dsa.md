# fuck bigO for not including growing memory access times as data grows
- [Cache oblivious DSA](https://cs.au.dk/~gerth/MassiveData02/notes/demaine.pdf)

# Sorting
- [Radix sort](https://probablydance.com/2016/12/27/i-wrote-a-faster-sorting-algorithm/)
- [Rsort impl](https://github.com/gorset/radix/blob/master/radix.cc)
- [Vector qsort](https://dl.acm.org/doi/pdf/10.1145/125826.126164)
- [Vector rsort](https://opensource.googleblog.com/2022/06/Vectorized%20and%20performance%20portable%20Quicksort.html)
- [Heap based](https://en.wikipedia.org/wiki/Partial_sorting)
- **Todo: benchmark these**

# PRNG
- [Comparisons](https://prng.di.unimi.it/)
- [Walk all permutations](https://cs.stackexchange.com/questions/153104/is-there-a-prng-that-visits-every-number-exactly-once-in-a-non-trivial-bitspace)

# Assoc. Arrays
- [Judy bench OLD](http://www.nothings.org/computer/judy/)
- [Judy](http://judy.sourceforge.net/)
- [Fibonacci Hashing](https://probablydance.com/2018/06/16/fibonacci-hashing-the-optimization-that-the-world-forgot-or-a-better-alternative-to-integer-modulo/)
- [Hash table](https://probablydance.com/2017/02/26/i-wrote-the-fastest-hashtable/)
- [Hash table 2](https://probablydance.com/2018/05/28/a-new-fast-hash-table-in-response-to-googles-new-fast-hash-table/)
- [Hash table test](https://github.com/goldsteinn/hashtable_test)
- [Optimizing hash table](https://8th-dev.com/forum/index.php/topic,1736.msg9653.html)
- Adapt to fixed size very low overhead stack-stored hash table
- **Todo: benchmark these**

# Mem mgmt
- [LuaJIT GC](http://wiki.luajit.org/New-Garbage-Collector#gc-algorithms_quad-color-optimized-incremental-mark-sweep)
- [Lobster memmgmt](https://aardappel.github.io/lobster/memory_management.html)