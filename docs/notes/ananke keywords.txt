types: 
any
	fun
	void
	u8[]
		str
		path
		cstr
	num
		int
			iX
			nat
				uX
		rat
			fX

declarations:
let var fun opr typ

values:
true false nil _ this

conditionals:
if elif else then case of check

loops:
for in while until do

control flow:
break redo skip fall goto return also defer eval continue resume

logical:
not and or imp

premptive multithreading:
fork join
