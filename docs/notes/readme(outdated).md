# Nouns, symbols

Ananke makes a distinction between a variable (symbol) and what the variable "holds" (a noun).

A variable is a symbol for a noun.

A noun can exist without a corresponding variable -> anonymous noun.

A noun is a discrete unit of data made up of bits.

Nouns have types and qualities.

Types are primitives, or derived from primitives via `using`, `struct`, `union`, `enum`.

Types have qualifiers which constrain them.

A fully qualified type = type + qualities.

Note that qualities can be an empty set.

FQ-type: fully qualified type

Operators act on whole nouns.



# Preprocessor

## Stack size

You MUST set the stack size for your program in bytes with `#stack x`.

	#stack 1048576 // 1MebiByte

The preprocessor may only reach a `stack` directive once. A file with

	#stack 1048576 // 1MebiByte
	#stack 1048576 // 1MebiByte

Will not compile.

**Note:** this practice is not necessary nor recommended for libraries.

## Including and linking

Including source files and/or directing the linker
`#include` will paste the files contents directly.

The following 4 directives can be chained with `#ifdef`, `#if`, etc to generate context-aware/portable compilation and linking options

	#include header.anh // paste header file contents
	#include source.ank // paste source file contents
	#archive archive.a // link archive file (static linking)
	#object object.o // link shared object file (dyanimc linking)

## pragma once

Pragma once is found in a lot of C and C++ code to make sure that source or header files are only included once. In Ananke, this is achieved by adding a program name directive. The compiler will only include a single instance of a named module.

	#name once_module

## Macros

Are sought and replaced before anything else.

	#define findstring replacewith
	#macro dosomething(x, y, ...) {
		printf(...);
		return x+y;
	}

## Default qualities

You can set a default quality like so:

	#default const
	#default owned
	#default native safe garbage 



# keywords

Keywords are reserved words that are tokenized as their own class of token. However, you might stupidly want to name your variable "u32" even though it is a keyword, or perhaps an external library written in another language uses a keyword. In that case, you can use the escape character `\`:

	u32 \u32 = 0;
	i16 \match = 666;
	*f32 \operator;

Metatype:
`typename`

Types:
`usize` `u8` `u16` `u24` `u32` `u64` `u80` `u128` `u256`
`isize` `i8` `i16` `i32` `i64` `i128` `i256`
`fsize` `f16` `f24` `f32` `f64` `f80` `f128` `f256`
`string`

Qualities:
`const` `mut`
`owned` `shared` `uniq` `safe`
`auto` `stack` `heap` `register` `retain` `garbage` `extern`
`native` `leo` `beo` `leb` `beb` `lolb` `lobb` `bolb` `bobb`

Logic:
`if` `elif` `else`
`match` `any` `fall`
`and` `or` `xor` `not` `true` `false`

Flow:
`for` `in` `to` `by` `while` `loop` `skip` `break`
`goto` `label`
`fn` `return`
`operator` `lassoc` `rassoc`
`construct` `iterate` `destruct`

Structure:
`struct` `union` `enum` `pad` `patch`
`namespace` `use` `as`

## Reserved

Not yet implemented. This list may change in the coming months.

Multithreading:
`thread` `yield` `atomic` `sema` `mutex` `crit` `async`

Memory management:
`destroy` `discard`

Flow:
`comefrom` `jumpto` `with` 

Qualities:
`volatile` `inline` `padded`

Error handling:
`try` `catch` `throw`



# Operators

**NOTE:** Biggest operators get parsed first, from left to right. `y+++++x` -> `y++ ++ +x` -> compilation fail

	types, modules
		.	fieldname resolver
	namespaces
		.	namespace resolver
	pointers
		$	address of (Where's the money Lebowski?)
		@	dereference (seek AT address)
		.	offset // access address + x bytes
		[]	access // access address + x*sizeof(@pointer) bytes
	arrays
		[]	access // arr[x] 
		..	range // a..b from a to b, including a and b
		,+..	seqarith // continue sequence a,b+..c until end of array or index c as if arithmetic, c is optional
		,*..	seqgeom // continue sequence a,b*..c until end of array or index c as if geometric, c is optional
	bitwise 	
		===	bits equals
		|	bits or
		&	bits and
		~	bits not
		^	bits xor
		$$	hamming distance
		<<	left shift
		>>	right shift

		^<<	left shift, keep first bit
		^>>	right shift, keep first bit
		<<^	left shift, keep last bit
		>>^	right shift, keep last bit
		<<<	left shift with carry bit
		>>>	right shift with carry bit

		.<	left rotate
		.>	right rotate

		+<	left rotate with carry bit
		+>	right rotate with carry bit

		(suffix/prefix operators)
		~~	reverse noun
		!!	inverse noun
		$!	parity
		?#	popcount, count number of 1 bits
		!#	inverse popcount, count number of 0 bits
		^?	offset of first 1 bit
		^!	offset of first 0 bit
		?^	offset of last 1 bit
		!^	offset of last 0 bit
	arithmetic
		+	plus
		-	minus
		/	divide
		*	multiply
		**	power
		++	increment
		--	decrement
		%	remainder (-21 % 4 = -1)
		%%	modulo (euclidian division remainder) (-21 %% 4 = 3)
	compare
		>	greater
		<	less
		>=	less or equal
		<=	greater or equal
		==	equal
		\=	not equal
	logic		
		!	not
		&&	and
		||	or
		^^	xor
	assignment
		=	is
		<=>	swap
	conversion
		:	convert
	ternary
		? : ,
	reserved, but available for operator extension
		=> ~> +? <>
		¦ † ‡ ¡ ¿ § ¶ ※
		¬ · × ÷ ± ‰

## Assignment operators

		=	is

You can also have compound assignment operators. `a+=b` for instance expands to `a = a+b`. Here is an _incomplete_ list of examples:

		!=	isnot
		+=	isplus
		-=	ismin
		*=	ismul
		/=	isdiv
		%=	isrem
		~=	isbnot
		^=	isbxor
		&=	isband
		|=	isbor
		.=	ismember isoffset
		->=	isfollow
		[]=	iselem
		<<=	islshift
		>>=	isrshift
		?=	isif
		!?=	isifn

## Operator extension

Operators can be _extended_:

	operator return assoc (lhs) op (rhs) {
		// code
	}

	operator f32 (i32 lhs) + (f32 rhs) {}
	// defines a + b;

	operator u8 ++ (u8 in) {}
	// defines ++a;

	operator u8 (u8 in) ++ {}
	// defines a++;

	operator u8 lassoc (u8 lhs) ** (u8 rhs) {}
	// defines a**b; with left-associativity.

By default, all binary operators only operate on two nouns of the same fq-type. Any attempt to mix and match will result in compilation failure.

For instance, scaling an array:

	operator i32[] (i32[] lhs) * (i32 rhs) {
		for elem in lhs {
			elem *= rhs;
		}
		return lhs;
	}
	i32[] arr = [1,2,3,4]*5; // [5,10,15,20,25]

Defining the `*` operator like this automatically defines its assignment operator `*=`.

**Note:** The `?` may not be extended.

**Note:** You may not define new operators. For instance, you can not create a 😂 operator.

**Note:** You may not extend the bitwise operators.

## Pre and post ops

Incrementing and decrementing:

	x++ post: add 1 and return old value of x
	++x pre: add 1 and return new value of x

Unlike in C, these are defined:

Assignment operators: RHS gets evaluated first; increments are done before assignments.

	x = x++
	x = x

	x = ++x
	x = x+1

	x += x++
	x = (x+1) + x

	x += ++x
	x = (x+1) + (x+1)

## boolean logic

keys: `true` `false`
ops: `!` `&&` `||` `^^`

There are no booleans in control, but nouns have a contingent property `true` or `false`, on which these operators operate.

A noun made up of only binary zeroes is considered `false`.

A noun, if not `false`, is considered `true`.

If a boolean expression resolves to return a new noun, the noun shall be equal to false=0 or true=1 as if it were an unsigned integer of the same endianness and size.

	i32 a = 1337;
	i32 b = 555;
	// a && b => native i32 1
	// a ^^ b => native i32 0

Boolean logic short circuits, allowing this:

	errorHasHappened && exit();

If errorHasHappened == false, exit() will not be evaluated (because a&&b with a=false will always be false, so no need to evaluate the right hand side).

## Ternaries

	cond ? iftrue;
	cond ? iftrue , eitherway;
	cond ? iftrue : iffalse;
	cond ? iftrue : iffalse , eitherway;

if x evals to `true`, return x, otherwise return a. Always execute, but don't return, c.

### Conditional overwrite

	data ?= x;
	data ?= fancyOverwriter();

if `data` is `true`, overwrite with `x`, otherwise keep `data`.

	data !?= x;

if `data` is `false` (!true), overwrite with `x`, otherwise keep `mydata`.



# Other characters

	parentheses     ( ) precedence, function arguments
	braces          { } code blocks
	brackets        [ ] arrays
	angled brackets < > templates
	semicolon       ;   statement seperator



# Literals

**Strings and letters**

 - String literal: enclosed in `""` or `''` or their fancy unicode equivalent.

Use the `\` escape character inside of a string or character literal to generate a code point. `"\x65"` means (ASCII) codepoint hex 65 (A), `"\u042e"` means unicode U+042e (=Ю). `\"` means the `"` character literally.

**Numbers**

 - Decimal point: `.`
 - Sign: `-` or absence of `-`
 - Seperator: `_`

Integer literals:

	'-'?[0-9_]+

Decimal literals:

	'-'?[0-9_]*[\.,][0-9_]*

The decimal literal `.` evaluates to `0.0`, as does `000_000_000.000_000`.



# Comments

No code is generated from these, during syntax tree generation they are removed.

	// comment until newline

	`
		multiline
		comment
	`

When a compiler finds the case insensitive strings "fixme", "todo" or "hack" in a comment body, it may do nothing, or issue a notice, a warning, or a snarky remark.



# Nouns

Data on computers is just a stream of bits. A 'Noun' in Ananke is a complete piece of data, to be treated as one whole. But arrays are also considered single nouns, which means that a noun can itself be composed of multiple nouns, just like "house" is a noun in English, and "brick" also.

Ananke has *types* and *qualifiers* to make sense of this: 

**The type gives the size of the noun as well as what it signifies:** 64-bit unsigned integer, 32-bit floating point value, etc. This makes sure operations translate to the correct assembly instructions.

**The qualifier describes the qualities of the noun:** is it a constant or variable? What is its endianness? Where is it to be stored: stack, heap, register?



# Variables

You can declare a variable by

	`type name;`

You can initialize it too:

	`type name = initializer;`

Or you can modify variables, call functions, etc:

	`name = modification;`
	`name = functioncall();



# Types

Ananke offers the following primitives from which you can construct all other types:

	umax u8 u16 u24 u32 u64 u80 u128 u256 u512
	imax i8 i16     i32 i64     i128 i256 i512
	fmax    f16 f24 f32 f64 f80 f128 f256
	string

 - The `yX` types are of X bits.
 - The `ysize` types are of the native address size. Ananke(usize) = C(size_t)

 - uX: unsigned
 - iX: signed two's complement

 - The `fX` types are IEEE binary floating points of X bits. There is no `f8` or `f512`. Note that `f16` is not the same floating point type that most ML engines use. `f80` is the highest native floating point precision of most AMD64 CPUs.

 - Strings are shorthand for `u8[]`.

**Note:** the type of a literal is deduced from the context.

## Conversion, Interpretation, Casting

**Conversion (`:`, `to`)** lets you modify the type while preserving meaning.

**Interpretation (` `, `as`)** lets you modify the type while preserving raw data.

You might want to turn a number into a string: that's conversion.

	float pi = 3.1415927;
	string pi_str = string:pi;
	string pi_str = pi to string;
	print(pi_str); // "3.1415927"

Or you might want to interpret the bits of a noun as if it were a type: that's interpretation. It might lead to garbage, but it can be handy when dealing with packets or complex structs.

	float pi = 3.1415927;
	u32 garbage = u32 pi;
	u32 garbage = pi as u32;

# Qualifiers
	
These say something about the noun, not the variable.

[default], alternative

## Mutability:

	[mut], const

 - `mut` means the noun is mutable, i.e. its data can be modified
 - `const` means the noun is constant, i.e. its data can not be modified

## Storage class:

	[auto], stack, register, heap, retain

 - `auto` lets the compiler choose
 - `stack` puts the variable in the stack frame
 - `register` puts the data in a register
 - `heap` puts the data on the heap (will **NOT** deallocate on scope exit)
 - `retain` puts the data in some place so that it is allocated once and stays allocated until program exit. The data will be kept through function calls. See example in functions chapter.

## Endianness:
	
	[native], leo, beo, leb, beb, lolb, lobb, bolb, bobb

 - `leo` little endian octet order (native bit order)
 - `beo` big endian octet order (native bit order)
 - `leb` little endian bit order (native octet order)
 - `beb` big endian bit order (native octet order)

 - `lolb` little octet little bit
 - `lobb` little octet big bit
 - `bolb` big octet little bit
 - `bobb` big octet big bit

Network byte order 32-bit integer for instace: `bobb i32`

x86-64 (AMD64) processor native byte order is `lobb`

For instance:

	lobb u32 num = 0xdeadbeef; // in memory [ef][be][ad][de]
	bobb u32 num = 0xdeadbeef; // in memory [de][ad][be][ef]

**Note:** incompatible types must be explicitly converted. `bobb i32 + lolb i32 = compilation error`

## Ownership:

Additionally, nouns have a special ownership "qualifier" available, `own`. 

	own i32 ownedNumber = 32;
	own* i32 ownedPointer; // C++ unique_ptr

`own` Makes sure that you do not destroy or reassign a noun after you have passed on ownership, for instance, to a function that takes an `own`ed noun as an argument. Rust calls this "borrow checking" and acts as if it is revolutionary. When used in conjuction with pointers, it implies memory responsibility.



# Pointers

`T*` `@` `$`

Pointers are a special type of noun, they are an adress to another noun. Declaring a pointer means declaring the type and qualities of the noun pointed to.

A pointer is declared with `*` and dereferenced with `@`.
An address is gotten with `$`var.

	* mypointer; // compilation fail
	i32* mypointer = ; // ok
	u8* unknownData; // ok

Pointer nouns always have the native endianness, so they are disqualified from the endianness qualifiers (but the nouns they point to aren't!). All other noun qualifiers are available. 



# References

Also called "Aliassing", this is _like_ a pointer but not quite: we are telling the compiler that the symbols a and b are equal and reference the same variable.

	a = &b;

Most times the compiler will compile this into a pointer behind the scenes. But sometimes it can get away with not doing so. Using references can thus be faster than pointers.



# Defining new types

You can define new types with the `using` keyword.

	using float = f32;
	float num = 2.5;



# Patching existing types

You may patch existing types, userdefined or standard, by adding extra methods or members. You can only patch-add members to structs, enums or unions.

	patch i32 {
		fn myMethod(){
			print("Waow! I am " + this as string);
		}
	}

	i32 num = 5;
	num.myMethod();



# Structures

Structuring data is of the essence. Here are 3 tools: struct, union, enum. They are never padded unless specified with the `pad` keyword.

## structs

Structs are your classes. They have properties and methods.

## unions

Unions are overlay types. You can define

	union ARGB
	{
		u32 color;
		struct {
			u8 b;
			u8 g;
			u8 r;
			u8 a;
		} components;
	} pixel;

In order to do

	pixel.colour = 0xff0402ff;
	pixel.components.a = 0x00;
	// pixel.color is now 0xff040200

## enums

Enums are variant types. It is a union with a way to keep track of the active element.

	enum WebEvent {
		PageLoad,
		PageUnload,
		u8 KeyPress,
		string Paste,
		i64[2] Click
	} myEvent;

	match myEvent {
		to PageLoad {}
		to PageUnload {}
		to KeyPress {print("Pressed "+myEvent.KeyPress as string);}
		to Paste {}
		to Click {}
	}

But they can also be used as plain enumerations:

	enum accountType {
		user,
		mod,
		admin,
		owner
	};

## default values

All can have default values.

Example usage:

	struct organism {
		u8[] name = "Nameless";
		enum sort {
			plant;
			animal;
			alien = 0;
		}
		union {
			f32 waterPerYear;
			u8 legCount = 4;
			u32 unknownData;
		}
	}

## Methods

All can have methods

	struct Thing {
		i32 x;

		fn setter(i32 arg){
			this.x = arg;
		}
	}

### iterate

structs can have iterators.

	struct listNode {
		listNode* next;
		i32 data;

		listNode* iterate {
			return this.next;
		}
	}

### destruct

structs can have destructors - even recursive.

	struct listNode {
		listNode* next;

		destroy {
			if next {
				destroy this.next;
			}
		}
	}

### construct

structs can have constructors

	struct dataHolder {
		i32[4] one;
		i32[4] two;

		construct (i32 a, i32 b){
			this.one = [b,a,b,a];
			this.two = [a,b,a,b];
		}
	}

	dataHolder myHolder = dataHolder(3,5);

### to

special method for converting types

	struct fraction {
		i32 num;
		i32 denom;

		to f32 {
			return (f32:num)/(f32:denom);
		}
	}

**Note:** by default, the "string" conversion is defined for every type, struct, etc. It will print in the following format: `MEMLOCATION struct{.member=val,.member=val}`, `MEMLOCATION enum{activemember=activememberval}`



# Namespaces

Classes are often used for namespacing. DON'T. DO. THAT! Use _namespaces_ for that.

	namespace myModule {
		const i32 someConstant = 42;

		fn someFunc(i32 n) {
			println("Hello, "+string:n+"!");
		}
	}

	myModule.someFunc(myModule.someConstant);

Here, the `using` keyword finds another use, unfolding a namespace:

	using myModule;
	someFunc(someConstant);



# Arrays

	[] [0] [#T] [..V]
	.len() .cap() .size()

Use array[n] to access the n-th member (0 based index!) of the array.

.size() gives the total size in bytes of the entire array-noun.

.len() gives the amount of elements in the array.

.cap() gives the amount of elements the array can hold.

## Array

An array is just a normal, growable array. Known in C++ as "vector". Since it is growable, it is by definition not constant: therefore it is disqualified from using the `const` variable.

	float[] array;

The internal element counter's type can also be specified, it is by default `usize`:

	float[u8] smallArray;

Checking for overflows is at the programmer's discretion:

	if smallArray.len() == smallArray.cap() {
		print("No more room!");
	} 

## Vector

A vector is a fixed size array. Known in C++ as "array". No, I didn't swap "vector" and "array" for fun - C++ is wrong in its terminology.

The compiler will attempt to automatically use vector SIMD instructions for vector nouns.

	float[3] vector;
	float[0] vector = {1,2,3} // fixed size array with size (3) determined by rhs expression

## Table

A table is a key-value hash map.

	string[#u64] table; // type(k,v) = type(string,u64)

## Sentinel sequence

A sentinel sequence is an array terminated by a specific _sentinel value_
	u8[..u8'0'] sequence; // equivalent to C's NUL-terminated "strings"

## Notes

 - All arrays are allocated, wherever they are allocated, as a continuous block, because they are a noun.

 - The address-of operator, `$`, when used on an array, shall return the address of the first byte of a noun. This means that it will not necesarily return the address of the first element, such is the case with dynamic arrays, where it returns the address of the element count.

 - Due to arrays being nouns, using a bitwise operator on an terminated array or dynamic array will also affect the terminator element or the count element respectively.

 - To convert a dynamically sized array to follow the same format as a statically sized array (i.e., drop the length prefix), you can convert like so: `type[0]:name`. Keep in mind you must manually allocate memory and keep count then.

 - Because arrays are nouns, assinging arrays **copies values** rather than a 'pointer to array' like in C.

## Ranges, sequences and initialization

The range operator `a..b` takes 2 numbers a and b and turns it into an array from a to b.

	u8[0] arr = [1..5];
	// arr = vector [1,2,3,4,5]

	u8[] arr = [1..5];
	// arr = array (5)[1,2,3,4,5]

	u8[..0] arr = [1..5];
	// arr = sent.seq. [1,2,3,4,5,0]

When we initialize an array with a pattern, we can continue the pattern with 

 - `a,b,c...` for a polynomial sequence
 - `a,b..+c` for a arithmetic sequence
 - `a,b..*c` for a geometric sequence

c is optional. Note that for polynomial sequences, a sufficient amount of numbers must be given.

	i32[5] arr = [1,2..+];
	// arr = [1,2,3,4,5]

	i32[5] arr = [1,2...];
	// arr = [1,2,3,3,3]

	i32[5] arr = [1,2..*];
	// arr = [1,2,4,8,16]

	i32 arr[] = [1,2..+4] // 4 cycles
	// arr = [1,2,3,4,5,6]

Because the compiler only looks at the last 2 elements. All of these methods return an array of whatever number type the first range operand is. The array is dynamic by default, but a static array can be created by putting these generators as an initializer of a static array.

## Methods

**Note:** if you need to string together multiple methods, it might be best to write a custom algorithm.

### size, len, cap

`array.size()` return the size of the array (including terminator or count) in **bytes**.

`array.len()` returns the amount of elements in the array.

 - For vectors this gets compiled away
 - For arrays and tables it is a O(1) lookup to the element count
 - For terminated arrays it is a O(n) lookup to count until the terminator is hit

`array.cap()` returns the capacity of the array. This includes used cells.

 - For vectors this gets compiled away
 - For arrays and tables it is a O(1) lookup to the internal structure
 - For terminated arrays it gets compiled away to `-1`

### contains, overlaps

`haystack.contains(needle)` returns `true` (u8 0x01) if all elements in `needle` can be found anywhere in `haystack`, or `false` (u8 0x00) if not.

`haystrack.overlaps(needle)` returns `true` (u8 0x01) if at least 1 element of needle can be found in `haystack`, or `false` (u8 0x00) if not.

### find, seek

Unlike `contains`, these search functions look for exact matches.

`haystack.seek(needle)` returns a dynamic array of 1 or 0 indices. 1 index at which `needle` can be found in `haystack` or 0 indices if `needle` can not be found. 

Couple `.seek` with `.elems` to get a true/false exact match result.

`haystack.find(needle)` returns a dynamic array of indices at which `needles` can be found. Empty array if `needle` can not be found in `haystack`.

### hamming, hammask

`array.hamming(array2)` returns the hamming distance between the arrays as a number.

`array.hammask(array2)` creates a new array where elements i are `true` if array[i] != array2[i], `false` otherwise.

### concat, union, intersect

`array.concat(array2)` returns 2 arrays concatenated back-to-back.

`array.union(array2)` returns a new array with all elements of `array`, and all not-already-present elements of `array2`. Otherwise back-to-back.

`array.intersect(array2)` returns a new array with all elements that exist in both `array` and `array2`. May be an empty array.

### deduplicate

`array.dedup()` returns a new array without duplications.

### sorting

`array.quicksort(comp)`

`array.mergesort(comp)`

`array.radixsort(comp)`

Where comp is a 2-callable (function, lamda, operator that takes 2 arguments) to sort by. If comp is not provided, it will be sorted in ascending order.

Comp MUST be provided if the array is not of primitives.

### pluck, extract

`haystack.pluck(needle_array)` returns a new array, haystack without any element that exists in needle. Relative complement.

`haystack.extract(needle_array)` returns haystack without any exact substring/subarray match of needle.

### reverse

`array.reverse()` reverses an array.

### insert, remove, add, push, shift, pop

`array.insert(elem, index)` inserts an element to a specific index, and shifts the rest of the array further back to accomodate it.

`array.remove(index)` removes a specific element from an array by index and shifts the rest of the array forward to fill up the gap.

`array.push(elem)` = insert(elem 0)

`array.shift()` = remove(0)

`array.shift(n)` removes first n elements

`array.add(elem)` = insert(array.len())

`array.pop()` = remove(array.len())

`array.pop(n)` removes last n elements

### map, zip, filter, reduce

**callable**: operator, function (lambda, method, literal function, etc)

 - 1-callable: callable that takes 1 input/arguments
 - 2-callable: callable that takes 2 inputs/arguments

### map

`map` will apply a 1-callable to each element in the array.

It maps a function to an array.

	`i32[] arr = [1..5].map(++);`
	// arr is [2,3,4,5,6]

### zip

`arr.zip(arr2, func)` will, given 2 same type and same length arrays `arr` and `arr2`, return a single array where each element is constructed by applying the 2-callable `func` to each pair of elements from the two arrays.

### filter

`filter` will, for each element of an array, keep or discard it based on a 1-callable applied to that element.

It filters the array.

	u8 fn even(i32 x) {
		return !(x%2);
	}
	arr = [2,3,4,5,6].filter(even);
	// arr will be [2, 4, 6]

### reduce

`reduce` will pass each element of an array to a 2-callable.

It reduces the array to a single thing.

	i32 sum = [2,4,6].reduce(+);
	// sum = 2+4+6 = 12

	arr.reduce(print) -> (print(print(2, 4)), 6) -> FAIL

As you can see, reduce produces nestling. `+`, which is `ADD(a,b)` can handle this but not `print`.

### chaining

We can chain all these expressions into one:

	i32 sum = (1..4).add(5).map(++).filter(even).reduce(+);

A good compiler will make this efficient, i.e. consolidate all the array returns. Armed with a bad compiler, you might be better off writing this sort of stuff manually.



# Functions

Function that returns an `i32` called "myFunc":

	i32 fn myFunc(args){code}

Call the function:

	i32 num = myFunc();

## Lambdas

Returns `i32`, called "myLambda"

	i32 fn(i32) myLambda = i32 fn(args){code}

## First class citizenship

Function that taken `i32` and returns (a function that takes `i32` and returns `i32`).

	(i32 fn(i32)) fn adderFactory(i32 mainArg) {
		return fn(i32 lamArg){
			return mainArg + lamArg;
		}
	}

	i32 fn(i32) increment = adderFactory(1);

	i32 five = increment(4);

## Many or No return type

func() takes no arguments and returns nothing.

	fn func(){code}

func2() takes no arguments and returns an `i32` and an `f32`.

	i32,f32 fn func2(){code}

	i32 x, f32 y = func2();

## polymorphism

func() is defined for multiple input types with different or same behavior.

	i32 fn func(i32 x){code1}

	f32 fn func(f32 x){code2}

	i32 y = func(3);
	f32 y = func(3.14);

## Variadic functions

A function can take an arbitrary amount of arguments with a `...x` argument. (that is, until a too-massive amount of arguments overflows the stack).

The function can access the varargs as a `u8[u64]`, a dynamic array of u8 with a 64-bit elem-count.

It is up to the programmer to extract and interpret the actual values and interpret their types in this array.

The reason for this limitation is that types are resolved at compile-time, not runtime. There is no interface for storing type information at runtime.

	fn variadic(...x){code;}



# Control flow

## goto

	label myLabel;
	print("Hello C64!");
	goto myLabel;

## if, elif, else

	if condition {
		code1;
	} elif otherCondition {
		code2;
	} else {
		code3;
	}

## match

Switch case, explicit fallthrough

	match(myVar){
		to 0 {
			print("var is 0");
		}
		to 1 {
			print("var is 1");
		}
		to 5 fall
		to 6 fall
		to 7 {
			print("Var is either 5, 6 or 7");
		}
		to any {
			print("var is anything else");
		}
	}

Multiple vars at once

	string fn fizzBuzz(i32 count){
		match(count%3, count%5){
			to 0,0 {
				return "FizzBuzz";
			}
			to 0, any {
				return "Fizz";
			}
			to any, 0 {
				return "Buzz";
			}
			to any, any {
				return count::string;
			}
		}
	}

## for

Traditional

	for i32 i=0 to 10 {code}

Increment size

	for i32 i=0 to 10 by 2 {code}

Compact

	for i32 i=0,10,2 {code}

Iterate
	
	for elem in array {code}

	for elem in array by 2 {code}

## loop
	
	// loop is do

	loop {
		print("I will loop forever");

	// skip is continue

		if condition1 {
			print("Skipping the rest of this loop iteration.");
			skip;
		}

	// break is break

		if condition {
			print("Breaking out of the loop.");
			break;
		}
	}

Loop in batches:

	loop 12 {
		print("I will be looped 12 times");
	}

## while

Loop with condition

	while condition {
		code;
	}

Every 12 cycles the condition is rechecked:

	while condition loop 12 {
		code;
	}

Check condition after instead of before:

	loop {
		code;
	} while condition;

	loop 12 {
		code;
	} while condition;



# templates

The best way to write type-agnostic generic code. Just insert `<template args>` right after the name of the function or structure.

	T fn func<type T>(T arg){code;}

	i32 num = func<i32>(5);

	struct data<type T, i32 Num>{
		T[Num] arr;
		T otherVar;

		method(T arg){
			this.arr[Num] = arg;
		}
	}
