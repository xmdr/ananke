1. Abstract value sets

> A boolean is either true or false, where "true" and "false" are hitherto undefined symbols.

```c
enum {true, false} boolean;
```

2. Concrete value sets

> A bit is either the value `0` or `1`.

```c
// no real C equivalent, this comes closest
enum {zero=0, one=1} bit;
```

3. Union of concrete and abstract value sets

> An optional type is either the type or the "value" nil

```c
typedef (???) T;
struct optional {
	enum {nil, the_type} kind;
	T value;
};
```

4. Union of concrete value sets

> An error type is either the type or any string

```c
typedef (???) T;
struct error {
	T success;
	char *error;
	// in pure C, the fact that error can be NULL can act as a discriminant
}
```

```ank
type Bool   = true/false
type Bit    = 0/1
type Opt[T] = T/nil
type Err[T] = Str err/T suc
```

you see, the / "operator" now has the grammar of being defined for declaration / declaration, type_expr / name, val_expr / val_expr, name/name
and of course in normal usage, val_expr / val_expr is already a think
so to make it work I'd need to define a completely new operator, or make a type-subgrammar
also name overlaps with both type_expr and val_expr 

ok, so 0 is really just a "name" for an abstract vaule
u1 is a name for a set of values
so really what you get is set/val val/val val/set set/set
set/val val/set and set/set can always be inferred to "return" a (bigger) set
val/val is context sensitive
context as in, which part of the grammar are we in
then at the tail end of analysis, each value gets assigned a concrete implementation (i.e., false becomes the number 0)
note to self: is this isomorphic to string interning? can the passes be combined? 
sometimes you want to predefine such an implementation, maybe type Bool = true=1/false=0

ok so fuck types
let's go with values and sets
```ank
val 1
val 0
val true = 1
val false = 0
set bool = [true,false]
let x in bool = 3; "type" error

; entity = cartesian product of the sets u32 and f32
set entity = health in u32, speed in f32
set entity_as_tuple = u32, f32
; err[T] = union of T and string
set err[T] = string / T
```
 
the type "string" can be seen as the union of all possible strings, and which variant it is, is decided by the value of an array of bytes (that's actually an implementation detail - let's just say a "natural number")
```c 
enum {apple = 0x6170706c65, banana=0x62616e616e61} string; 
```

and boom, that gives "free" string interning
as long as you implement "value crushing" where you crush down the implementation of a value down to its smallest possible size in bits
value crushing would also give pointer compaction for free assuming you use regions for memory allocation

next there's value fusion. For instance, the C example error struct has undergone value fusion between the variant values and the NULL value of str, based on the observation that they are inherently interlinked/puns: kind would never be error if str=NULL. There's a 2 way implication: str==NULL <=> kind==error

u32 would then be more of a "ensure any value in this set can be crushed down to 32 binary digits"

Every literal is just an alternative notation for an abstract value

so let's work in sets and symbols and concrete values
```ank
val 0 = Natural(0)
sym false = 0
val 1 = Natural(1)
sym true = 1
set bool = [0,1]

val banana_str = Natural(0x62616e616e61)
sym "banana" = banana_str
set string = ["banana", ...]
```
anyway, crushing would occur at the level of "sym"
the optimizer would make a bijection on the values of the syms in a set and a new set of (smaller) values
for instance,
```ank
set fruit_str = ["apple", "banana", "cherry"]
; equivalent to syms
set fruit_str = [apple, banana, cherry]
; equivalent to vals
set fruit_str = [xNat(6170706c65), xNat(62616e616e61), xNat(636865727279)]
; but these vals would also suffice
set fruit_str = [Nat(0), Nat(1), Nat(2)]
```
