Consider the memory management of a compiler during parsing. We have a parent pointer tree for scopes containing dynamic hash tables and the abstract syntax tree is, well, a tree which also contains references: for each node in the AST to the then-current scope.

```c
typedef struct table {
	struct {
		char data[128]; // imagine some symbol info like maybe a type or something
		char *key;
	} *entries;
	int num_entries;
	int capacity;
} Table;

typedef struct scope {
	struct scope *parent;
	struct table *symbols;
} Scope;

// meandering about algebraic datatypes aside,
typedef struct ast {
	char data_stuff[128]; // imagine an enum for which kind of node and other auxilary data
	struct scope *scope;
	struct ast *children;
	int num_children;
} AST;

Scope **free_ast_node(AST *root){
	Scope **scope_pointers; // imagine a dynamic array to keep track of all the scope pointers we encounter as we destroy the AST
	for child in root->children {
		merge(scope_pointers, free_ast_node(child));
	}
	insert(scope_pointers, root->scope);
	free(root);
	return scope_pointers;
}

void free_table(Table *t){
	for entry in t->entries {
		free(entry.key); // lets just assume no string is aliased, but I _could_ go on all day like this collecting an array of key_pointers
	}
	free(t->entries);
	free(t);
}

void free_all(AST *root){
	Scope **scope_pointers = free_ast_node(root);
	for scope in dedup(scope_pointers) {
		free_table(scope->symbols);
		free(scope);
	}
}
```

But one can see that parent pointer tree nodes can be kept in a single array, single allocation.

```c
static Scope *scopes; // realloced in parser
static int num_scopes = 0;
static int scopes_capacity = 0;

typedef struct ast {
	char data_stuff[128];
	struct ast *children;
	int num_children;
	int scope; // index into array
} AST;
```

We can now avoid the code for collecting all scope pointers, and just `free(scopes)` at the end.

An even smarter programmer can see that all hash tables can be combined into a giant hash table by giving the entries an additional `scope_id` which is hashed along with the string. This means less dynamically growing objects to worry about: we now just have a single dynamic "entries" array to worry about, with ad-hoc/implied/ephemeral scopes, as well as a single array for defining parent relationships. AST nodes carry an `int scope_id`. Now we only need to free the AST tree.

```c
#include <stdint.h>
typedef uint32_t u32;
typedef uint64_t u64;
typedef const char *str;

typedef struct {
	char data[128];
} Entry;

typedef struct __attribute__((packed)) {
	str key;   // the string keys
	u64 hash;  // what the key+scope hashes to
	u32 scope; // scope id
	u32 entry; // index into entries (value)
	u32 psl;   // robin hood hashing: probe sequence length
} KVPair;

typedef struct {
	KVPair kvpairs;
	Entry *entries; // the actual values
	u32    symamt;  // amt of symbols
	u32    symcap;  // cap of symbols

	u32 *parent;    // parent[12] == 2 means scope 2 is the parent of scope 12
	u32  scopeamt;  // amt of `parent`
	u32  scopecap;  // cap of `parent`
} Symtab;

typedef struct __attribute__((packed)) {
	void *data;
	u32  *children; // indexes into `nodes`
	u32   childamt; // amt of children
	u32   scope;
} AST;

typedef struct {
	Symtab symtab;
	AST    *nodes; // AST node storage
	u32    astamt; // amt of nodes
	u32    astcap; // cap of nodes
} Program;

typedef struct {
	Program prog;
	str src;
	struct {
		str srcname;
		u32 line;
		u32 col;
	} loc;
} Parser;

#define SYMTAB_SLACK 16
void init(Parser *p, const char *source, const char *srcname){
	const unsigned default_symcap   = 512;
	p->prog.symtab.kvpairs = calloc(default_symcap+SYMTAB_SLACK, sizeof(*p->prog.symtab.kvpairs));
	p->prog.symtab.entries = calloc(default_symcap, sizeof(*p->prog.symtab.entries));
	p->prog.symtab.symamt = 0;
	p->prog.symtab.symcap = default_symcap;
	
	const unsigned default_scopecap = 128;
	p->prog.symtab.parent = calloc(default_scopecap, sizeof(*p->prog.symtab.parent));
	p->prog.symtab.scopeamt = 1;
	p->prog.symtab.scopecap = default_scopecap;

	const unsigned default_nodecap  = 1024;
	p->prog.ast.scope = 0;
	p->prog.nodes = calloc(default_nodecap, sizeof(*p->prog.symtab.nodes));
	p->prog.astamt = 0;
	p->prog.astcap = default_nodecap;

	p->src = source;
	p->loc.srcname = srcname;
	p->loc.line = 1;
	p->loc.col = 1;
}

static inline
void growtab(Symtab *t){

}

static inline
u64 hash_key(str key, u32 len, u32 seed){
	// assumes strings are 8 byte aligned AND ZERO PADDED

	// seed the hash
	u64 h = seed;

	// convert to array of u64
	u64  len8 = (len+7)/8;
	u64 *key8 = (u64*)(void*)key;

	for(u64 i=0; i<len8; i++){
		h += i+key8[i];
	}

	// fibbonacci hashing
	// round((2^64)/((1 + sqrt(5)) / 2)) - 1
	h *= 11400714819323198485llu;

	return h;
}

str newstr(const char* s, u32 l){
	u64 len = (l+7)/8;
	char *x = calloc(l,sizeof(char));
	memcpy(x, s, l);
	return x;
}

static inline
u64 getidx(Symtab *t, str key, u32 len, u32 scope){
	u64 hash  = hash_key(key, len, scope);
	u64 index = hash & (t->symcap-1); // hash % t->symcap
	return index;
}

static inline
Entry *symins(Symtab *t, str key, u32 len, u32 scope){
	u64 start = getidx(t, key, keylen, scope);
	
}

// get an entry for a key of specific scope
static inline
Entry *symget(Symtab *t, str key, u32 len, u32 scope){
	u64 start = getidx(t, key, keylen, scope);
	for(u64 i=start; i<SYMTAB_SLACK; i++){
		Entry *e = t->entries+i;
		if (i > e->psl) return NULL;
		if (e->scope == scope && strcmp(e->key, key) == 0){
			return e;
		}
	}
	return NULL;
}

// get an entry for a key
static inline
Entry *lookup(Symtab *t, str key, u32 len, u32 scope){
	int keylen = strlen(key);

	for(;;) {
		Entry *res = symget(key, keylen, scope);
		if (res){
			return res; // found
		}
		if (scope == 0){
			return NULL; // not found
		}
		scope = scopes.parent[scope];
	};
}
```