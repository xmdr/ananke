Time lib

type calendar
type instant
type period
type datetime
String rep/field type (day mo yr)

A `period` is a quantity of a unit of time.
an `instant` is a normalized position in time of a given resolution.
op `instant + period` produces a new `instant` of the lowest resolution necessary to facilitate lossless addition into an integer result.
`instant` can be converted into a string rep or a field type, in which case timezones, dst, leaps, are all resolved via a `calendar` type.
The `calendar` type stores which timezone, dst, leaps and even formatting rules.
A `datetime` is a `calendar` and `instant` bundled together.

	let n = now() /. returns an instant ./
	let c = calendar{}.fromZoneInfo("Europe/Amsterdam")
	let d = datetime{n,c}
