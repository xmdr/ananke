## Why Ananke is awesome

### Philosophy

**Ananke** is a reasonable-batteries included **systems programming** language, with an **imperative** core, but strong hints of **functional** and **array based** programming, and a **high-level** sheath. With some snippets, you'd be forgiven if you thought it was a dynamic language. It follows the C ethos: **"the programmer knows best"** and does everything it can to empower you, the programmer.

### It compiles fast, it runs even faster

Ananke is a compiled language, meaning it cuts out a lot of computational middle men. It's very **resource efficient**, CPU and RAM wise, and using Ananke _will_ **save you server costs**.

LOC/s compile time: 20000 loc/s

Targets:
 - WASM
 - x86-64(bare, windows, linux)
 - ARM64(bare, linux)
 - Local interpreter
 
### Everything is an expression

**Everything is an expression**. This leads to great syntactical freedom and expressivity. Oh, and **no semicolons**. A terse yet friendly syntax in general designed for programmer ergonomics. It has imperative roots, but strong hints of functional and array based programming.

```
/. this is a /. nested ./ comment ./
; another comment
use rng
let relation = rng.rand(0,1) > 0.5
	then "larger than"
	else "smaller than or equal to"
printl("the random value was \{relation} 0.5.")
```

### Type system

Static duck type system- feels dynamic, **doesn’t get in your way**, great for prototyping, but **protects against shooting yourself in the foot**. Ananke tries to generate the appropriate **algebraic datatypes inferred** from how and where you use variables. For robust software you can constrain and specify the types as much as you want, and the `assume` and `where` clauses are particularly nifty. There are also **sum types**, **product types**, **parametric types** and the **unit type** `void` for those who enjoy total specification, as well as **ad-hoc polymorphism** for those who know what that means.

```
let x = 5
int y = x
str z = x; type error, int != string
str w = x to str; type conversion
let f = \a print("\{a+1}"); lambda of one arg
f(w); type error, operator string + int doesn't exist
f(x); ok
f(4.5); works as well: duck typing provides ad-hoc polymorphism for floating point/integer types
let g = fn(int a){print("\{a+1}")}
; we can use g to force an integer usage
g(4.5); type error, 4.5 is not int
g(4); ok

; ad-hoc polymorphism:
fn poly(str x){printl("A string: "..x)}
fn poly(int x){printl("An int: \{x}")}
poly("hello world!"); "A string: hello world"
poly(5); "An int: 5"
```

### Pattern matching

Ananke has powerful **pattern matching** syntax, `switch`' older brother, which works great with the type system.

```
let mixed_arr = ["test", 4, 23.3, nil]
let elem = rng.pick_rand(mixed_arr);
printl("The element is ".. elem of {
	str x: "a string: \{x}",
	int x: "an integer: \{x}",
	rat x: "a rational: \{x}",
	_: "something else"
})
```

### Continuations and pointers

Ananke has **continuations**, the cradle of all control flow, like pointers are for data structures. You can re implement all control flow including function returns, but also **coroutines, exceptions, resumable exceptions**, get creative! `break` breaks out and continues execution to where the current scope `{}` ends. Exception: scopes following branches (`if`, `else`, `elif`, `then`) are not counted, the parent is broken out of instead. You can also use **labeled breaks**.

```
var i = 0
fn continuation(var x){
	printl("got \{x}")
	break
}
; this will loop forever
continuation(i+=1); increment i at every call
```

Oh, Ananke has **raw pointers** to. All power to you.

```
var x = "this"
let ptr = x@; postfix address-of operator
ptr$ = "that"; postfix dereference operator
printl(x); "that"
```

### Meta-programming

Ananke has **compile time evaluation** of any expression, as well as **procedural macros** that can manipulate the AST. This means the language is open ended and extensible, and you can **extend Ananke to cater to your exact needs** by writing Ananke.

```
fn fib(i) {
	i < 1 then 1 else fib(i-1)+fib(i-2)
}
let x = eval fib(40); x is calculated completely at compile time. The code for the fibonacci function isn't even generated.

use ananke
; all macros are prefix operators
; `::` is the initializer operator
macro modify(ananke.expression x){
	return ananke.ast::{
		kind: ananke_assign,
		children: [x, ananke.ast::{kind: ananke_integer, value: 4}]
	}
}
var hello = 5
#modify hello

; transformed into:
var hello = 5
hello = 4
```

### Concurrency/parallelism

Ananke also has built-in primitives for **concurrency**, with a focus on awaitable futures. There are **no colored functions**, you can execute any expression asynchronously. In Ananke, async has nothing to do with state machines or coroutines, it is a completely orthogonal feature that concerns only the branching of timelines, i.e., **parallelism**. The implementation of #async and #await are open ended and you can make them yourself or import a libary. A new thread for each? A threadpool? fork()? You decide! There's also a **robust standard library of locks, monitors, signals,** etc.

```
let future = #async fib(40); we calculate fib(40) concurrently.
let fib40 = #await future
; for all numbers 2 to 12, calculate the fibonacci of that number, asynchronously per number
arr = 2..12.map(\x #async fib(x))
; `x.=y` -> `x=x.y`
arr.= map(\x #await x); transform `arr` into an array of all results
```

### Error handling

Error handling is up to you. You can do Go-style error handling where you check **optional types** for `nil`, or you can use continuations to create **(resumable) exceptions**, or you can use the more Rust-flavored **error types** `error<T>`. Ananke offers the nil-safe `?` and nil-coalesce `??` operators which also work for `error<T>`.

### Unicode

Ananke is a **Unicode-first** language. This is reflected in the standard string type and library, which are all geared for UTF-8, the encoding standard of today. Ananke has **no nul-terminator**, making string handling faster and raw manipulation safer.

### Lifetimes

Novel lifetime system - you can declare something with an explicitly passed lifetime parameter, such as the lifetime of the caller. This prevents some need for “output” parameters. Ananke also offers multiple returns values.

```
fn f(){
	fn<life L> g(){
		int<L> x = 4
		int y = 34
		return x$, y
	}
	; this is all safe
	let ptr, val = g<now>()
	@ptr = 5
	printl("\{@ptr} and \{val}")
}
```

### Ergonomic function handling

Ananke has **uniform call syntax**, **first class functions**, function composition, lamdas (but regular function notation is also just an expression) and **closures**. It also has user defined operators and first class operators (they are just functions with funky notation after all).

### Linear algebra - vectors, matrices are native
```
let M = [[1,2,3],
         [4,5,6],
         [7,8,9]]
let I = [[1,0,0],[0,1,0],[0,0,1]]
let R = M*I; matrix multiplication
let U = [1,2,3]
let V = [4,5,6]
let W = U.cross(V); cross product
let x = U*V; dot product
```

### Built in hash tables and array primitives
```
let animal_counts = ["dog":4, "cat":2, "mouse":2]
animal_counts["ferret"] = 3
for count, animal in animal_counts {
	printl("\{animal}: \{count}")
}
```

### Exact, bit for bit, data layout precision and execution

The `exact` and `litend`/`bigend` keywords are a godsend. Yes, you can finally overlay/pun several data types by using `exact` layouts, without any nasty surprises or undefined behavior. This is super useful for sending and receiving binary buffers of data. Write out the spec for a data structure, fill it in, **write it directly to disk then read it back again, _it just works._**

You can also **tell the compiler to not optimize your code** by prefixing an expression (that includes code blocks!) with `exact`, which is important to cryptographic libraries - no more `volatile` hacks!

```
exact u8[] data = {0x34, 0x30, 0x00, 0x2c, 0x34, 0x39, 0x38, 0x96}
exact type my_struct {litend i32 x, litend f32 y}
let translated = data as my_struct; bit reinterpret cast
; translated now holds a copy of data
translated.x; we can access this without UB
translated.y; same here
; we could also safely alias `data` and `translated` by using pointers.
```