#include <stdio.h>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <gvc.h>

static cairo_status_t read_png_stream(void *closure, unsigned char *data, unsigned int length) {
    GInputStream *stream = (GInputStream *)closure;
    gssize bytes_read;
    GError *error = NULL;

    bytes_read = g_input_stream_read(stream, data, length, NULL, &error);
    if (bytes_read == -1) {
        g_print("Failed to read from input stream: %s\n", error->message);
        g_error_free(error);
        return CAIRO_STATUS_READ_ERROR;
    }

    return CAIRO_STATUS_SUCCESS;
}

void draw_graph(GtkWidget *widget, cairo_t *cr, gpointer user_data) {
    Agraph_t *graph;
    GVC_t *gvc;
    unsigned char *data;
    unsigned data_len;

    // Create a new directed graph
    graph = agopen("MyGraph", Agdirected, NULL);

    // Add nodes to the graph
    Agnode_t *node1 = agnode(graph, "Node 1", 1);
    Agnode_t *node2 = agnode(graph, "Node 2", 1);
    Agnode_t *node3 = agnode(graph, "Node 3", 1);

    // Add edges to the graph
    agedge(graph, node1, node2, "Edge 1-2", 1);
    agedge(graph, node2, node3, "Edge 2-3", 1);
    agedge(graph, node3, node1, "Edge 3-1", 1);

    // Create a Graphviz context
    gvc = gvContext();

    // Layout the graph
    gvLayout(gvc, graph, "dot");

    // Render the graph to a data buffer
    gvRenderData(gvc, graph, "png", (char **)&data, &data_len);

    // Create a GInputStream from the rendered data
    GMemoryInputStream *stream = g_memory_input_stream_new_from_data(data, data_len, g_free);

    // Create a Cairo surface from the input stream
    cairo_surface_t *surface = cairo_image_surface_create_from_png_stream(read_png_stream, (void *)stream);
    cairo_set_source_surface(cr, surface, 0, 0);
    cairo_paint(cr);
    cairo_surface_destroy(surface);

    // Cleanup
    g_object_unref(stream);
    gvFreeLayout(gvc, graph);
    agclose(graph);
    gvFreeContext(gvc);
}

int main(int argc, char **argv) {
    GtkWidget *window;
    GtkWidget *drawing_area;

    // Initialize GTK
    gtk_init(&argc, &argv);

    // Create the main window
    window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_default_size(GTK_WINDOW(window), 400, 300);
    gtk_window_set_resizable(GTK_WINDOW(window), FALSE);
    gtk_window_set_title(GTK_WINDOW(window), "Graph Drawing Example");
    g_signal_connect(window, "destroy", G_CALLBACK(gtk_main_quit), NULL);

    // Create a drawing area
    drawing_area = gtk_drawing_area_new();
    gtk_container_add(GTK_CONTAINER(window), drawing_area);

    // Connect the draw_graph function to the "draw" signal of the drawing area
    g_signal_connect(drawing_area, "draw", G_CALLBACK(draw_graph), NULL);

    // Show all the widgets
    gtk_widget_show_all(window);

    // Start the GTK main loop
    gtk_main();

    return 0;
}
