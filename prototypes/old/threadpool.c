#include <stdlib.h>
#include <pthread.h>
#include <stdatomic.h>

/*
1. async: enqueue task {function, payload, barrier}
2. one of the workers is finished, picks up the task out of queue
3. once work is finished, worker unlocks the barrier
4. await: blocked by the barrier. Once through, destroys task
*/

typedef struct {
	void(*task)(void *);
	void *payload;
	pthread_mutex_t unlockWhenDone;
} Task;

static struct pool{
	pthread_t *workers;
	Task **taskArray; // first in last out, for now
	size_t taskArrayLen;
	size_t taskArrayCap;
	pthread_mutex_t taskArrayLock; // tbd: concurrent queue
} globalPool;


int work(){
	while(1){
		// removes the task in front and returns it
		pthread_mutex_lock(&globalPool.taskArrayLock);
		Task *t = globalPool.taskArray[--globalPool.taskArrayLen];
		pthread_mutex_unlock(&globalPool.taskArrayLock);
		
		// executes task
		t->task(t->payload);

		// releases the tasks barrier
		pthread_mutex_unlock(&t->unlockWhenDone);
	}
}

// submits a task
void async(void (*task)(void*), void *payload){
	// "payload" has already been allocated
	// initializes the task
	Task *t = malloc(sizeof(*t));
	t->task = task;
	t->payload = payload;
	pthread_mutex_init(&t->unlockWhenDone, NULL);
	pthread_mutex_lock(&t->unlockWhenDone);

	// emplaces it in the back of the pool's queue
	pthread_mutex_lock(&globalPool.taskArrayLock);
	if (globalPool.taskArrayLen == globalPool.taskArrayCap){
		globalPool.taskArray = realloc(globalPool.taskArray, 2*globalPool.taskArrayCap*sizeof(*globalPool.taskArray));
	}
	globalPool.taskArray[globalPool.taskArrayLen++] = t;
	pthread_mutex_unlock(&globalPool.taskArrayLock);
}

// destroys a task
void *await(Task *t){
	// awaits the barrier
	pthread_mutex_lock(&t->unlockWhenDone);

	// destroys the task while keeping the payload
	void *p = t->payload;
	free(t);
	pthread_mutex_unlock(&t->unlockWhenDone);
	pthread_mutex_destroy(&t->unlockWhenDone);
	return p;
}
