#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

typedef enum {
	t_as, t_to, t_while, t_if, t_else, t_elif, t_do, t_break, t_skip, t_fall, t_case, t_of,
	t_type, t_let, t_var, t_and, t_or, t_not,
	t_fun, t_ident, t_lparen, t_rparen, t_lbrace, t_rbrace, t_lbracket, t_rbracket,
	t_for, t_in, t_excl, t_tilde, t_amp, t_hash, t_dollar, t_pct, t_caret, t_et,
	t_star, t_min, t_plus, t_line, t_eq, t_colon, t_dot, t_comma, t_slash,
	t_qst, t_bslash, t_lt, t_gt, t_eof, t_num
} type;

typedef struct {
	type type;
	void* data;
} token;

char *cpchar(char x){
	char *s = malloc(2);
	s[0] = x;
	s[1] = 0;
	return s;
}

char* input = "fun main(int[] x){for e in x {print(e)}}";

int main(int argc, char* argv[]) {
	token tokens[100];
	int token_index = 0;
	char* p = input;

	// Lookup table for single character tokens
	static type lut[] = {
		['('] = t_lparen, [')'] = t_rparen, ['{'] = t_lbrace, ['}'] = t_rbrace,
		['['] = t_lbracket, [']'] = t_rbracket, ['!'] = t_excl, ['~'] = t_tilde,
		['@'] = t_amp, ['#'] = t_hash, ['$'] = t_dollar, ['%'] = t_pct,
		['^'] = t_caret, ['&'] = t_et, ['*'] = t_star, ['-'] = t_min,
		['+'] = t_plus, ['_'] = t_line, ['='] = t_eq, [':'] = t_colon,
		['.'] = t_dot, [','] = t_comma, ['/'] = t_slash, ['?'] = t_qst,
		['\\'] = t_bslash, ['<'] = t_lt, ['>'] = t_gt
	};

	// Keyword table
	struct {
		char* ident;
		type type;
	} keywords[] = {
		{"fun", t_fun},
		{"for", t_for},
		{"in", t_in},
	};
	int num_keywords = sizeof(keywords) / sizeof(keywords[0]);

	while (*p) {
		// Skip white space and comments
		while (isspace(*p) || *p == ';' || (*p == '/' && *(p+1) == '.')) {
			if (*p == ';') {
				// Skip line comment
				while (*p && *p != '\n') p++;
			} else if (*p == '/' && *(p+1) == '.') {
				// Skip multiline comment
				p += 2;
				int level = 1;
				while (*p) {
					if (*p == '/' && *(p+1) == '.') {
						level++;
						p += 2;
					} else if (*p == '.' && *(p+1) == '/') {
						level--;
						p += 2;
						if (level == 0) break;
					} else {
						p++;
					}
				}
			} else {
				p++;
			}
		}

		if (isalpha(*p) || *p=='_') {
			// Parse identifier
			char* ident_start = p;
			while (isalnum(*p) || *p=='_') p++;
			int ident_length = p - ident_start;
			char* ident = malloc(ident_length + 1);
			memcpy(ident, ident_start, ident_length);
			ident[ident_length] = '\0';

			int is_keyword = 0;
			for (int i = 0; i < num_keywords; i++) {
				if (strcmp(ident, keywords[i].ident) == 0) {
					tokens[token_index++] = (token){keywords[i].type, keywords[i].ident};
					is_keyword = 1;
					free(ident);
					break;
				}
			}
			if (!is_keyword) {
				tokens[token_index++] = (token){t_ident, ident};
			}
		} if (isdigit(*p)) {
			
		} else {
			type t = lut[(int)*p];
			if (t != 0) {
				tokens[token_index++] = (token){t, cpchar(*p)};
			} else if (*p == '\0') {
				tokens[token_index++] = (token){t_eof, 0};
				break;
			}
			p++;
		}
	}

	for (int i = 0; i < token_index; ++i) {
		printf("%s\n", (char*)tokens[i].data);
	}
}