#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

// A ByteReader is an iterator that reads bytes.
// It is a view of the data. i.e. it does not own the underlying data, i.e. it is not responsible for (de)allocation.
// Calling next() when hasNext() is false is undefined behavior.
// The implementations all have a ByteReaderInterface as their first member ensuring overlapping memory layouts -> safe to "downcast".
typedef struct ByteReaderInterface ByteReaderInterface;

// Return true if there is a next byte, otherwise false
typedef bool (*const HasNext)(ByteReaderInterface*);

// Return the next byte in the iterator
typedef uint8_t (*const Next)(ByteReaderInterface*);

struct ByteReaderInterface {
	HasNext hasNext;
	Next next;
};

// A StringIterator is a ByteReader over a character array
typedef struct {
	ByteReaderInterface bri;
	const uint8_t *str;
} StringIterator;
uint8_t StringIterator_next(ByteReaderInterface *bri){
	StringIterator *si = (StringIterator*)bri;
	uint8_t byte = *si->str;
	si->str++;
	return byte;
}
bool StringIterator_hasNext(ByteReaderInterface *bri){
	StringIterator *si = (StringIterator*)bri;
	return si->str[0] != 0;
}
StringIterator StringIterator_new(const uint8_t *s){
	return (StringIterator){
		.bri = (ByteReaderInterface){
			.hasNext = StringIterator_hasNext,
			.next    = StringIterator_next,
		},
		.str = s
	};
}

// A FileIterator is a ByteReader over the contents of a file
typedef struct {
	ByteReaderInterface bri;
	FILE* f;
} FileIterator;
uint8_t FileIterator_next(ByteReaderInterface *bri){
	FileIterator *fi = (FileIterator*)bri;
	return fgetc(fi->f);
}
bool FileIterator_hasNext(ByteReaderInterface *bri){
	FileIterator *fi = (FileIterator*)bri;
	int character = fgetc(fi->f);
	if (character != EOF){
		ungetc(character, fi->f);
		return true;
	}
	return false;
}
FileIterator FileIterator_new(FILE *f){
	return (FileIterator){
		.bri = (ByteReaderInterface){
			.hasNext = FileIterator_hasNext,
			.next    = FileIterator_next
		},
		.f = f
	};
}
