#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
#include <stdnoreturn.h>

#include "bytereader.h"

// Thrown by a CodepointReader
noreturn void invalidUnicodeException(void){
	fprintf(stderr, "%s\n", "INVALID UNICODE");
	int UNICODE=0,VALID=1;
	assert(UNICODE == VALID);
	exit(1);
}

uint32_t countLeadingOnes(uint8_t num){
	return __builtin_clz(~((uint32_t)num)<<(32-8));
}

// A CodepointReader is an iterator over a ByteReader, returning whole utf8-encoded unicode codepoints.
// It employs the strategy pattern: we don't care how we get the bytes as long as we get them through _some_ ByteReader.
typedef struct CodepointReader CodepointReader;
struct CodepointReader {
	bool (*const hasNext)(CodepointReader*);
	uint32_t (*const next)(CodepointReader*);
	ByteReaderInterface *const byteReader;
};
bool CodepointReader_hasNext(CodepointReader *r){
	return r->byteReader->hasNext(r->byteReader);
}
// Fetch the next unicode codepoint in a utf8 stream
uint32_t CodepointReader_next(CodepointReader *r){
	ByteReaderInterface *br = r->byteReader;
	if (!br->hasNext(br)){
		return 0;
	}
	uint8_t b = br->next(br);
	uint32_t lo = countLeadingOnes(b);
	uint32_t code;
	if (lo == 0){
		code = b;
	} else if (lo == 1){
		// continuation byte??
		invalidUnicodeException();
	} else if (lo <= 4){
		uint32_t remaining = lo-1;
		code = b & (0x7F >> lo);
		do {
			if (!br->hasNext(br)){
				invalidUnicodeException();
			}
			b = br->next(br);
			// continuation byte expected: 10xxxxxx
			if ((b & 0b11000000) == 0b10000000){
				code = (code << 6) | (b&0b00111111);
			} else {
				invalidUnicodeException();
			}
		} while(--remaining);
	} else {
		// no utf8 is more than 4 bytes
		invalidUnicodeException();
	}
	return code;
}
CodepointReader CodepointReader_new(ByteReaderInterface *bri){
	return (CodepointReader){
		.hasNext    = CodepointReader_hasNext,
		.next       = CodepointReader_next,
		.byteReader = bri
	};
}
// Print a codepoint as a utf8 string
void printCodepoint(uint32_t c) {
	printf("U+%04X: ", c);
	if (c <= 0x7F) {
		// For code points in the range U+0000 to U+007F, it is a single-byte UTF-8 character
		putchar(c);
	} else if (c <= 0x7FF) {
		// For code points in the range U+0080 to U+07FF, it is a two-byte UTF-8 character
		putchar(0xC0 | ((c >> 6) & 0x1F));
		putchar(0x80 | (c & 0x3F));
	} else if (c <= 0xFFFF) {
		// For code points in the range U+0800 to U+FFFF, it is a three-byte UTF-8 character
		putchar(0xE0 | ((c >> 12) & 0x0F));
		putchar(0x80 | ((c >> 6) & 0x3F));
		putchar(0x80 | (c & 0x3F));
	} else if (c <= 0x10FFFF) {
		// For code points in the range U+10000 to U+10FFFF, it is a four-byte UTF-8 character
		putchar(0xF0 | ((c >> 18) & 0x07));
		putchar(0x80 | ((c >> 12) & 0x3F));
		putchar(0x80 | ((c >> 6) & 0x3F));
		putchar(0x80 | (c & 0x3F));
	} else {
		// Invalid codepoint (outside of valid Unicode range)
		invalidUnicodeException();
	}
}

// Example usage
int main() {
	// Sunglasses
	// Family: Man, Woman, Girl, Girl (with zero width joiner)
	// Vulcan Salute: Dark Skin Tone (just a postfix skin tone modifier)
	uint8_t data[] = "Hello world! 😎👨‍👩‍👧‍👧🖖🏿!";
	StringIterator si = StringIterator_new(data);
	CodepointReader cpr = CodepointReader_new((ByteReaderInterface*)&si);
	while (cpr.hasNext(&cpr)){
		uint32_t codepoint = cpr.next(&cpr);
		printCodepoint(codepoint);
		puts("");
	}

	const char *filename = "testfile.txt";
	FILE *f = fopen(filename, "rb");
	if (f == NULL) {
	    printf("Failed to open the file: %s\n", filename);
	    perror("Error"); // Print the error message provided by the OS
	    return 1;
	}
	FileIterator fi = FileIterator_new(f);
	CodepointReader cpr2 = CodepointReader_new((ByteReaderInterface*)&fi);
	while (cpr2.hasNext(&cpr2)){
		uint32_t codepoint = cpr2.next(&cpr2);
		printCodepoint(codepoint);
		puts("");
	}

	fclose(f);
	return 0;
}
