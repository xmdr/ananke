#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

// debug levels:
// 0 -> no debug
// 1 -> fatal errors
// 2 -> noise
#define DEBUG_LVL 1
#define debug(lvl,...) {if(lvl<=DEBUG_LVL)fprintf(stderr,__VA_ARGS__);}

#ifndef NO_VAL
#define NO_VAL NULL
typedef void* VAL_T;
#endif

// hash function to turn a string into a number. Yoinked from stackoverflow.
size_t hash(const char *str){
	size_t hash = 5381;
	int c;

	while ((c = *str++)){
		hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
	}

	return hash;
}

typedef struct{
	char* key;
	VAL_T val;
	size_t keyhash;
} kvpair;

typedef struct{
	size_t  cap; // capacity
	size_t  occ; // occupancy
	kvpair *arr; // k,v array
} Table;

// void tab_dump(Table *t){
// 	for(size_t i=0; i < t->cap; i++){
// 		kvpair p = t->arr[i];
// 		printf("%lu:\t\"%s\"(%lu) -> %p\n", i, p.key, p.keyhash, p.val);
// 	}
// }

// finds an empty slot in the key value pair array $arr[$len], starting from $start.
size_t find_empty(kvpair *arr, size_t len, size_t start){
	// find empty slot, starting from $start
	//debug(2,"finding in arr[%lu] at %p from %lu\n",len,arr,start);
	size_t idx, off = 0; 
	do {
		//idx = (start+off++)%len;
		idx = (start+off++)&(len-1); // power of two trick
		//debug(2,"trying slot %lu\n",idx);
		if (off == len){
			//debug(1,"could not find empty slot??\n");
			exit(EXIT_FAILURE);
		}
	} while(arr[idx].key != NULL);

	return idx;
}

// initialize the hash table $t
void tab_init(Table *t, size_t size){
	//debug(2,"initializing table at %p\n",t)
	t->occ = 0;
	if (size < 16 || (size&(size-1)) != 0){
		// too small size will break our occupation measurement in tab_add()
		// power of two required for lookup
		//debug(1,"provided size (%lu) too small or not a power of two!\n",size)
		exit(EXIT_FAILURE);
	}
	t->cap = size;
	t->arr = calloc(size, sizeof *t->arr);
	if (t->arr == NULL){
		//debug(1,"could not allocate kv array\n");
		exit(EXIT_FAILURE);
	} else {
		//debug(2,"allocated arr[%lu] at %p\n",t->cap,t->arr);
	}
}

// resizes and reindexes a given table $t
void tab_resize(Table *t, size_t size){
	// grow the kvpair array to $size
	//debug(2,"resizing arr[%lu] at %p to %lu\n",t->cap,t->arr,size);
	kvpair *newarr = calloc(size, sizeof *newarr);
	if (newarr == NULL){
		//debug(1,"could not allocate kv array\n");
		exit(EXIT_FAILURE);
	}

	// reindex table
	for (size_t oi = 0; oi < t->cap; ++oi){
		if (t->arr[oi].key == NULL){
			continue;
		}
		size_t ni = find_empty(newarr, size, hash(t->arr[oi].key));
		newarr[ni].key = t->arr[oi].key;
		newarr[ni].val = t->arr[oi].val;
	}

	free(t->arr);
	t->cap = size;
	t->arr = newarr;
	//debug(2,"resized arr[%lu] at %p\n",t->cap,t->arr);
}

// adds a given key,value pair $key, $val to table $t
void tab_add(Table *restrict t, char *restrict key, VAL_T val){
	//debug(2,"attempting to insert (\"%s\"->%p) ",key,val);
	//debug(2,"into t: %p (o: %lu c: %lu)\n",t,t->occ,t->cap);
	//if (t->occ*10 > t->cap*8){ // 80% occupancy
	if (t->occ*2 > t->cap){ // 50% occupancy
		size_t newsize = t->cap*2;
		//debug(2,"growing table to %lu\n",newsize);
		tab_resize(t,newsize);
	}
	size_t kh = hash(key);
	size_t idx = find_empty(t->arr, t->cap, kh);
	//debug(2,"inserting \"%s\"->(%p) at %p[%lu]\n",key,val,t->arr,idx);
	t->arr[idx].key = key;
	t->arr[idx].val = val;
	t->arr[idx].keyhash = kh;
	t->occ++;
}

// gets the index of a given key $key inside of table $t
size_t tab_idxof(Table *restrict t, const char *key){
	const size_t hsh = hash(key);
	const size_t len = t->cap;
	size_t off=0;
	do {
		//const size_t idx = (hsh+off)%len;
		const size_t idx = (hsh+off)&(len-1);
		if (t->arr[idx].keyhash == hsh && strcmp(t->arr[idx].key, key) == 0){
			return idx;
		}
	} while (off++ < len);
	//debug(2,"Key \"%s\" does not exist in table %p",key,t);
	exit(EXIT_FAILURE);
}

// retrieves the value of a given key $key
VAL_T tab_get_or_die(Table *restrict t, const char* key){
	return t->arr[tab_idxof(t, key)].val;
}

VAL_T tab_get(Table *restrict t, const char* key){
	const size_t hsh = hash(key);
	const size_t len = t->cap;
	size_t off=0;
	size_t idx;
	VAL_T noval = NO_VAL;
	do {
		//const size_t idx = (hsh+off)%len;
		idx = (hsh+off)&(len-1);
		//debug(2,"Trying %lu for \"%s\"\n",idx,key);
		if (t->arr[idx].keyhash == hsh && strcmp(t->arr[idx].key, key) == 0){
			return t->arr[idx].val;
		}
		//debug(2,"Didn't find\n");
	} while (memcmp(&t->arr[idx].val, &noval, sizeof(VAL_T))!=0 && off++ < len);
	return NO_VAL;
}

// sets a given key $key to value $val
VAL_T tab_set(Table *restrict t, const char* key, VAL_T val){
	size_t idx = tab_idxof(t, key);
	VAL_T old = t->arr[idx].val;
	t->arr[idx].val = val;
	return old;
}

// deletes a given key $key from a table $t
// returns what was at that key
VAL_T tab_del(Table *restrict t, const char* key){
	size_t i = tab_idxof(t,key);
	t->arr[i].key = NULL;
	return t->arr[i].val; // not really necessary to set to NULL
}
