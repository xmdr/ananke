#include <stdio.h>
#include <stdlib.h>

#include "parser.c"

int main(int argc, char const *argv[]){
	if (argc==2){
		// parse a file
		size_t slen;
		const char *src;
		const char *fname = argv[1];
		if (!readfile(&src, &slen, fname)){
			puts("error reading file");
			exit(1);
		}
		parse_prog(src,fname);
		free((void*)src);
	} else {
		// parse this
		parse_prog(
	"for x in arr { print(x) }"
	,"inline source"
		);
	}
}
