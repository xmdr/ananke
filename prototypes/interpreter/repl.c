// note: link with -lreadline

#include <stdio.h>
#include <stdlib.h>

/* If we are compiling on Windows compile these functions */
#ifdef _WIN32
	#include <string.h>

	static char buffer[2048];

	/* Fake readline function */
	char* readline(char* prompt) {
		fputs(prompt, stdout);
		fgets(buffer, 2048, stdin);
		char* cpy = malloc(strlen(buffer)+1);
		strcpy(cpy, buffer);
		cpy[strlen(cpy)-1] = '\0';
		return cpy;
	}

	/* Fake add_history function */
	void add_history(char* unused) {}

	/* Otherwise include the editline headers */
#else
	#include <editline/readline.h>
	//#include <editline/history.h>
	// compile with -ledit
#endif

// https://www.coolgenerator.com/ascii-text-generator
// shadow theme
const char *greet = "\
     \\                          |          \n\
    _ \\    __ \\    _` |  __ \\   |  /   _ \\ \n\
   ___ \\   |   |  (   |  |   |    <    __/ \n\
 _/    _\\ _|  _| \\__,_| _|  _| _|\\_\\ \\___| \n\
\n\
Welcome to Ananke v0! Press ctrl+c to exit.\n\
To copy/paste: ctrl+shift+c and ctrl+shift+v.\n\
(Blame terminal emulators for that)\n";

#include "parser.c"

const char *getUserName(const char* envp[]){
	for (int i = 0; envp[i] != NULL; i++){
		const char *match;
		if (match=strstr(envp[i], "LOGNAME=")){
			return match+strlen("LOGNAME=");
		}
		return "programmer";
	}
}

int main(int argc, const char *argv[], const char *envp[]) {
	printf(greet);//, getUserName(envp));
	while (1) {
		/* Now in either case readline will be correctly defined */
		char *input = readline("\n> ");
		add_history(input);

		parse_prog(input,"inline");
	}

	return 0;
}
