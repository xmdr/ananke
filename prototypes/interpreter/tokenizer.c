#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <assert.h>
#include "common/readfile.c"

/*
TOKENIZER
The tokenizer is a stateful coroutine-like design:
a state struct (Lexer) with an advancement function (lexer_consume)

- lookahead is stored in Lexer.next
- current token is stored in Lexer.curr
- the tokenizer also takes a symbol table (hash table) to correctly categorize recurring identifiers.
*/

// what kind of identifier is it?
typedef enum{
	v_unknown  = 0, // not seen before
	v_type     = 1, // refers a type
	v_keyword  = 2, // a keyword
	v_other    = 3, // refers any other variable except below
	v_function =-1, // refers a function
	v_preop    =-2, // refers a prefix operator
	v_postop   =-3, // refers a postfix operator
	v_infop    =-4, // refers an infix operator
	v_macro    =-5, // refers a macro
} ident_kind;
#define INVOKABLE_IDENT 0x8000
// x&0x8000 or x<0 to see if callable

// set up hash table (C generics, yeah)
typedef struct {
	ident_kind k;	
} VAL_T;
#define NO_VAL (VAL_T){v_unknown}
#include "common/table.c"

typedef enum {
	t_eof=0,t_none,t_unused,
	t_ident, t_float, t_int, t_string, t_lambda,
	t_lparen, t_rparen, t_lbrace, t_rbrace, t_lbracket, t_rbracket, t_comma,

	t_lt, t_gt, t_assign, t_neq, t_plus, t_minus, t_dot, t_deref, t_addr, t_safe, t_pow, t_bnot, t_mult, t_div, t_mod, t_tilde, t_band, t_bor,
	t_lrot, t_rrot, t_comp, t_concat, t_mod2, t_mmult, t_paamayim_nekudotayim, t_popcnt, t_lzcnt, t_coalesce, t_floordiv, t_div2, t_eq, t_gteq, t_lteq, t_rshift, t_lshift, t_incr, t_decr, t_hash, t_colon,
	/* length:count [keywords] */
	/* 1:1  */ t_any,
	/* 2:10 */ t_as, t_co, t_do, t_fn, t_if, t_in, t_it, t_op, t_or, t_to,
	/* 3:11 */ t_and, t_for, t_let, t_nil, t_nor, t_not, t_ref, t_str, t_use, t_var, t_xor,
	/* 4:18 */ t_atom, t_bool, t_case, t_cont, t_elif, t_else, t_eval, t_from, t_goto, t_here, t_pure, t_skip, t_that, t_this, t_true, t_type, t_with, t_void,
	/* 5:11 */ t_async, t_await, t_break, t_check, t_exact, t_false, t_macro, t_trash, t_where, t_while, t_yield,
	/* 6:8  */ t_bigend, t_export, t_import, t_litend, t_module, t_return, t_static, t_thread,
	/* 7:0  */
	/* 8:1  */ t_volatile,
	NUM_TOKENS
} Token;
static const char* toklut[] = {"end of input","no token","unused token","identifier","decimal number","integer","string","\\","(",")","{","}","[","]",",","<",">","=","!=","+","-",".","t_deref","t_addr","?","^","!","*","/","%","~","&","|","<<<",">>>","<=>","..","%%","**","t_paamayim_nekudotayim","t_popcnt","t_lzcnt","??","//","/%","==",">=","<=",">>","<<","++","--","#",":","_","as","co","do","fn","if","in","it","op","or","to","and","for","let","nil","nor","not","ref","str","use","var","xor","atom","bool","case","cont","elif","else","eval","from","goto","here","pure","skip","that","this","true","type","with","void","async","await","break","check","exact","false","macro","trash","where","while","yield","bigend","export","import","litend","module","return","static","thread","volatile"};

// returns the correct keyword token, or the t_ident token if not a keyword
Token which_keyword(char *str, size_t len){
	if (len > 8){
		// there are no keywords longer than 8 chars
		return t_ident;
	}
	static const char *keys[10] = {
		"","_",
		"ascodofnifinitoporto",
		"andforletnilnornotrefstrusevarxor",
		"atomboolcasecontelifelseevalfromgotoherepureskipthatthistruetypewithvoid",
		"asyncawaitbreakcheckexactfalsemacrotrashwherewhileyield",
		"bigendexportimportlitendmodulereturnstaticthread",
		"YUMMY!!",
		"volatile",
	};

	// LUT for no. of keywords with length [idx]
	static size_t numkwds[9] = {0, 1, 10, 11, 18, 11, 8, 0, 1};

	// LUT for enum value offset of keyword in bucket [idx]
	static int offsets[9] = {0, t_any, t_as, t_and, t_atom, t_async, t_bigend, 0, t_volatile};

	const char *bucket = keys[len];

	size_t ch = 0, kwd = 0;
	while (ch < len && kwd < numkwds[len]) {
		const int diff = bucket[kwd*len+ch] - tolower(str[ch]);
		if (diff > 0){
			// letter_kwd > letter_str
			// sorted means we will never reach keyword now
			return t_ident;
		}
		else if (diff < 0){
			// letter_kwd < letter_str
			// keyword may be somewhere in front of us
			kwd++;
		}
		else {
			// letters match
			if (++ch == len){
				// last letter mean a complete keyword!
				return (Token)(offsets[len]+kwd);
			}
		}
	}

	return t_ident;
}

typedef struct {
	Token token;
	int len;
	struct {
		const char *file;
		int line, col;
	} pos;
	union {
		double v_decimal;
		long v_integer;
		char *v_string; // owned
		struct{
			ident_kind variant;
			char *name; // owned
		} v_ident;
	} value;
} Lexeme;
void print_lexeme(Lexeme l);

typedef struct {
	int line;
	const char *fname;
	const char *head;
	const char *linestart;
	Lexeme curr,next;
} Lexer;

Lexeme lexer_consume(Lexer* l, Table* c);

Lexer lex(const char *stream, const char *fname){
	const Lexeme niltok = (Lexeme){t_none};
	Lexer l = (Lexer){
		.line=0,
		.fname=fname,
		.head=stream,
		.linestart=stream,
		.curr=niltok,
		.next=niltok,
	};
	lexer_consume(&l, NULL); // next=set, curr=null
	return l;
}

#define RED "\e[0;31m"
#define NC "\e[0m"
#define errout(...) fprintf(stderr, __VA_ARGS__)
#define ERROR(...) errout(LOC);errout(" [ERROR] "__VA_ARGS__)

// consumes one token
// returns the new Lexer.curr
// *c is the symbol table/context
Lexeme lexer_consume_internal(Lexer *l, Table *c){
	l->curr = l->next;
	int len = 0;

	#define POS {l->fname, 1+l->line, l->head-l->linestart}
	#define LOC "%s:%d:%lu",l->fname, l->line, l->head-l->linestart

	lex_top:
	// end of stream
	if (*l->head == '\0'){
		l->next = (Lexeme){t_eof, 0, POS};
		return l->curr;
	}
	
	// get whitespace out of the way
	while(isspace(*l->head)){
		if(*l->head++=='\n'){
			l->line++;
			l->linestart = l->head;
		}
		goto lex_top;
	}

	// get comments out of the way
	while (strncmp(l->head, "/.", 2)==0){
		int nest = 0;
		l->head+=2;
		do {
			if (strncmp(l->head, "/.", 2)==0){
				nest++;
				l->head+=2;
			} else if (strncmp(l->head+len, "./", 2)==0){
				nest--;
				l->head+=2;
			} else if (*l->head++=='\0'){
				ERROR("unterminated comment");
				exit(1);
			}
		} while(nest>=0);
		len=0;
		goto lex_top; // clear any leftover whitespace
	}

	// line comment
	if(*l->head==';'){
		do {
			l->head++;
		} while(*l->head != '\n' && *l->head != '\0');
		goto lex_top;
	}

	// see if we can find a word
	if (isalpha(*l->head) || *l->head=='_' ||*(const unsigned char*)l->head>127){
		const char *wstart = l->head;
		do {
			len++;
		} while (isalnum(l->head[len])||l->head[len]=='_'||*(const unsigned char*)&l->head[len]>127);
		Token t = which_keyword((void*)wstart, len);
		l->next = (Lexeme){t, len, POS};
		l->head += len;
		if (t == t_ident){
			char *name = calloc(1,len+1);
			memcpy(name, wstart, len);

			// which kind of identifier?
			if (c == NULL){
				l->next.value.v_ident.variant = v_unknown;
			} else {
				l->next.value.v_ident.variant = tab_get(c, name).k;
			}

			l->next.value.v_ident.name = name;
		}
		return l->curr;
	}

	// maybe a number
	int decimal = 0;
	if (isdigit(*l->head) || (*l->head=='.'&&(decimal=1))){
		do {
			len++;
		} while( isdigit(l->head[len]) || (l->head[len]=='.'&&!decimal++) );
		if (l->head[len-1]=='.'){
			// for uniform call syntax, we must not count a trailing "."
			if (len==1){
				goto not_a_number;
			}
			decimal = 0;
			len--;
		}
		l->next = (Lexeme){.len=len,.pos=POS};
		if (decimal){
			sscanf(l->head, "%lf", &l->next.value.v_decimal);
			l->next.token = t_float;
		} else {
			sscanf(l->head, "%li", &l->next.value.v_integer);
			l->next.token = t_int;
		}
		l->head += len;
		return l->curr;
	}
	not_a_number:

	// maybe a string
	if (*l->head == '"'){
		len++;
		int slen=0;
		while (l->head[len] != '"'){
			if (l->head[len] == '\0'){
				ERROR("unterminated string");
				exit(1);
			}
			slen++; len++;
		}
		len++;
		char *contents = calloc(1,slen+1);
		memcpy(contents, l->head+1, slen);
		l->next = (Lexeme){t_string, len, POS, .value.v_string=contents};
		l->head += len;
		return l->curr;
	}

	// an operator
	static struct{
		int l;
		const char *s;
		Token t;
	} lut[] = {
		#define OP(x,t) {sizeof(x)-1,x,t}
		OP("<<<", t_lrot), OP(">>>", t_rrot), OP("<=>", t_comp), OP("..", t_concat), OP("%%", t_mod2), OP("**", t_mmult), OP("::", t_paamayim_nekudotayim), OP("~$", t_popcnt), OP("~@", t_lzcnt), OP("??", t_coalesce), OP("//", t_floordiv), OP("/%", t_div2), OP("==", t_eq), OP("!=", t_neq), OP(">=", t_gteq), OP("<=", t_lteq), OP(">>", t_rshift), OP("<<", t_lshift), OP("++", t_incr), OP("--", t_decr), OP("+", t_plus), OP("-", t_minus), OP(".", t_dot), OP("$", t_deref), OP("@", t_addr), OP("?", t_safe), OP("^", t_pow), OP("!", t_bnot), OP("*", t_mult), OP("/", t_div), OP("%", t_mod), OP("~", t_tilde), OP("&", t_band), OP("|", t_bor), OP("=", t_assign), OP("(", t_lparen), OP(")", t_rparen), OP("{", t_lbrace), OP("}", t_rbrace), OP("[", t_lbracket), OP("]", t_rbracket), OP("<", t_lt), OP(">", t_gt), OP(",", t_comma), OP("->", t_unused), OP("<-", t_unused), OP("~>", t_unused), OP("<~", t_unused), OP("...", t_unused), OP("'", t_unused), OP("#", t_hash), OP("`", t_unused), OP("\\", t_lambda), OP(":", t_colon)
		#undef OP
	};
	const int LUTLEN = sizeof(lut)/sizeof(lut[0]);
	for (size_t i=0; i<LUTLEN; i++){
		if (strncmp(l->head, lut[i].s, lut[i].l)==0){
			l->next = (Lexeme){lut[i].t, lut[i].l, POS};
			l->head += lut[i].l;
			return l->curr;
		}
	}

	// not a token
	ERROR("invalid character \\x%x near \"%.6s\" at %p!\n", *l->head, l->head, l->head);
	exit(1);
}
Lexeme lexer_consume(Lexer *l, Table *c){
	Lexeme lex = lexer_consume_internal(l,c);
	//printf("Lexer: curr=");print_lexeme(l->curr);
	return lex;
}

void print_lexeme(Lexeme l){
	printf("%s", toklut[l.token%114]);
	switch(l.token){
		case t_int:
		printf("(%li)", l.value.v_integer); break;
		case t_float:
		printf("(%lf)", l.value.v_decimal); break;
		case t_string:
		printf("(\"%s\")", l.value.v_string); break;
		case t_ident:
		printf("(\"%s\", context=%d)", l.value.v_ident.name, l.value.v_ident.variant); break;
		default:break;
	}
	printf(" (%s:%d:%d)\n", l.pos.file, l.pos.line, l.pos.col);
}

void lex_everything(Lexer *l, Table *c){
	while(lexer_consume(l, c).token != t_eof){
		//printLexeme(l.curr);
		if(l->curr.token == t_string){
			free(l->curr.value.v_string);
		} else
		if(l->curr.token == t_ident){
			free(l->curr.value.v_ident.name);
		}
	}
}
