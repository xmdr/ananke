#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// dictionary
// hash function to turn a string into a number. Yoinked from stackoverflow.
size_t hash(const char *str){
	size_t h = 5381;
	
	size_t c;
	while ((c = *str++)){
		h = ((h << 5) + h) + c; //hash * 33 + c
	}

	return h;
}

typedef struct{
	const char *key;
	void *val;
	size_t keyhash;
} kvpair;

typedef struct{
	size_t  cap; // capacity
	size_t  occ; // occupancy
	kvpair *arr; // k,v array
} Table;

void tab_dump(Table *t){
	for(size_t i=0; i < t->cap; i++){
		const kvpair p = t->arr[i];
		if (p.key){
			printf("%lu:\t\"%s\" (%lu) -> %p\n", i, p.key, p.keyhash, p.val);
		}
	}
}

// finds an empty slot in the key value pair array $arr[$len], starting from $start.
size_t find_empty(kvpair *arr, size_t len, size_t start){
	// find empty slot, starting from $start
	size_t idx, off = 0; 
	do {
		//idx = (start+off++)%len;
		idx = (start+off++)&(len-1); // power of two trick
		if (off == len){
			exit(EXIT_FAILURE);
		}
	} while(arr[idx].key != NULL);

	return idx;
}

// initialize the hash table $t
void tab_init(Table *t, size_t size){
	t->occ = 0;
	if (size < 16 || (size&(size-1)) != 0){
		// too small size will break our occupation measurement in tab_add()
		// power of two required for lookup
		size = 16;
	}
	t->cap = size;
	t->arr = calloc(size, sizeof *t->arr);
	if (t->arr == NULL){
		exit(EXIT_FAILURE);
	}
}

// resizes and reindexes a given table $t
void tab_resize(Table *t, size_t size){
	// grow the kvpair array to $size
	kvpair *newarr = calloc(size, sizeof *newarr);
	if (newarr == NULL){
		exit(EXIT_FAILURE);
	}

	// reindex table
	for (size_t oi = 0; oi < t->cap; ++oi){
		if (t->arr[oi].key == NULL){
			continue;
		}
		size_t ni = find_empty(newarr, size, hash(t->arr[oi].key));
		newarr[ni].key = t->arr[oi].key;
		newarr[ni].val = t->arr[oi].val;
		newarr[ni].keyhash = t->arr[oi].keyhash;
	}

	free(t->arr);
	t->cap = size;
	t->arr = newarr;
}

// adds a given key,value pair $key, $val to table $t
void tab_add(Table *restrict t, const char *restrict key, void *val){
	if (t->occ*10 > t->cap*8){ // 80% occupancy
		size_t newsize = t->cap*2;
		//debug(2,"growing table to %lu\n",newsize);
		tab_resize(t,newsize);
	}
	size_t hsh = hash(key);
	size_t idx = find_empty(t->arr, t->cap, hsh);
	t->arr[idx].key = key;
	t->arr[idx].val = val;
	t->arr[idx].keyhash = hsh;
	t->occ++;
}

// gets the index of a given key $key inside of table $t
size_t tab_idxof(Table *restrict t, const char *key){
	const size_t hsh = hash(key);
	const size_t len = t->cap;
	size_t off=0;
	do {
		//const size_t idx = (hsh+off)%len;
		const size_t idx = (hsh+off)&(len-1);
		if (t->arr[idx].keyhash == hsh && strcmp(t->arr[idx].key, key) == 0){
			return idx;
		}
	} while (off++ < len);

	// does not exist
	return 0xffffffffffffffff;
}

void *tab_get(Table *restrict t, const char* key){
	size_t idx = tab_idxof(t, key);
	if (idx == 0xffffffffffffffff){
		return NULL;
	} else {
		return t->arr[idx].val;
	}
}

// deletes a given key $key from a table $t
// returns what was at that key
void *tab_del(Table *restrict t, const char* key){
	size_t i = tab_idxof(t, key);
	if (i == 0xffffffffffffffff){
		return NULL;
	}
	t->arr[i].key = NULL;
	return t->arr[i].val; // not really necessary to set to NULL
}

int parse(const char *prog);

typedef enum {tfor, tin, tcase, tof, tto, tas, tdefer, tcheck, teval, tit, tuntil, treturn, ttype, tlet, tvar, tnil, tdo, tgoto, tbreak, tskip, tredo, tfall, tif, telif, tthen, telse, twhile, tfun, tand, tor, talso, tnot, tint, trat, tnum, tnat, tstr, ttrue, tfalse, texact, tstruct, tuse, texport, timport, tmod, tvolatile, tbool, tatom, tvoid, tlitend, tbigend, tnative, tmacro} toktype;

typedef struct {
	toktype tktype;
} Token;

Table keywords;

void tok_ident(const char *s){

}

#include <ctype.h>

void tokenize(const char *prog, Token *arr, size_t *cap, size_t *len){
	const char *i = prog;
	while(*i){
		while (isspace(*i++))

		if (isalpha(*i)){
			// identifier
			const char *start = i++;
			while (isalnum(*i++));
			const char *end = i;
			size_t len = end-start;
			char *key = malloc(len);
		}
		if (isdigit(*i)){
			
		}
	}
}

int main(){
	Table *p = &keywords;
	tab_init(p, 16);
	tab_add(p, "for", &(Token){tfor});
	tab_add(p, "in", &(Token){tin});
	tab_add(p, "case", &(Token){tcase});
	tab_add(p, "of", &(Token){tof});
	tab_add(p, "to", &(Token){tto});
	tab_add(p, "as", &(Token){tas});
	tab_add(p, "defer", &(Token){tdefer});
	tab_add(p, "check", &(Token){tcheck});
	tab_add(p, "eval", &(Token){teval});
	tab_add(p, "it", &(Token){tit});
	tab_add(p, "while", &(Token){twhile});
	tab_add(p, "until", &(Token){tuntil});
	tab_add(p, "fun", &(Token){tfun});
	tab_add(p, "return", &(Token){treturn});
	tab_add(p, "type", &(Token){ttype});
	tab_add(p, "let", &(Token){tlet});
	tab_add(p, "var", &(Token){tvar});
	tab_add(p, "nil", &(Token){tnil});
	tab_add(p, "do", &(Token){tdo});
	tab_add(p, "goto", &(Token){tgoto});
	tab_add(p, "break", &(Token){tbreak});
	tab_add(p, "skip", &(Token){tskip});
	tab_add(p, "redo", &(Token){tredo});
	tab_add(p, "fall", &(Token){tfall});
	tab_add(p, "if", &(Token){tif});
	tab_add(p, "elif", &(Token){telif});
	tab_add(p, "then", &(Token){tthen});
	tab_add(p, "else", &(Token){telse});
	tab_add(p, "and", &(Token){tand});
	tab_add(p, "or", &(Token){tor});
	tab_add(p, "not", &(Token){tnot});
	tab_add(p, "also", &(Token){talso});
	tab_add(p, "int", &(Token){tint});
	tab_add(p, "rat", &(Token){trat});
	tab_add(p, "num", &(Token){tnum});
	tab_add(p, "str", &(Token){tstr});
	tab_add(p, "nat", &(Token){tnat});
	tab_add(p, "true", &(Token){ttrue});
	tab_add(p, "false", &(Token){tfalse});
	tab_add(p, "exact", &(Token){texact});
	tab_add(p, "struct", &(Token){tstruct});
	tab_add(p, "use", &(Token){tuse});
	tab_add(p, "export", &(Token){texport});
	tab_add(p, "import", &(Token){timport});
	tab_add(p, "mod", &(Token){tmod});
	tab_add(p, "volatile", &(Token){tvolatile});
	tab_add(p, "bool", &(Token){tbool});
	tab_add(p, "atom", &(Token){tatom});
	tab_add(p, "void", &(Token){tvoid});
	tab_add(p, "macro", &(Token){tmacro});
	tab_add(p, "litend", &(Token){tlitend});
	tab_add(p, "tbigend", &(Token){tbigend});
	tab_add(p, "native", &(Token){tnative});



	void *x = tab_get(p, "else");
	printf("%d==%d\n", *(int*)x, telse);

	tab_dump(p);
	free(keywords.arr);
}