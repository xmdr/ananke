static u32 *parent;
static BST *root;
static size_t n_scopes = 0;

void *sym_find(u32 scope, u32 symbol){
	static_assert((uintptr_t)NULL == (uintptr_t)0, "compiled can not be compiled on this platform (because it sucks (despite C standard compliance))");
	
	assert(scope < n_scopes);
	
	void *found = NULL;
	do {
		found = bst_find(bst_root[scope], symbol);
		scope = parent[scope];
	} while (!found && scope);
	return found;
}

